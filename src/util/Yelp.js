// import SearchBar from "../components/SearchBar/SearchBar";

const apiKey = '1IJ1X8jpccLgFa19qIMn3VzfvGmr81Vv0wFvc4zHvjpi1fIQR-ngyfe6OZjt8EgNZzngqwELDJHg8fkWl_xsgE5Ai2pwh5 - 3YDvEmxfy3M9dT2RaX25IA38Nzc9lYHYx';

const Yelp = {
    
    searchYelp(term, location, sortBy) {
        return fetch(`https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`, {
            headers: {
                Authorization: `Bearer ${apiKey}`
            }
        }).then((response) => {
            return response.json();
        }).then((jsonResponse) => {
            if(jsonResponse.businesses){
                return jsonResponse.businesses.map(business => ({ 
                        id: business.id,
                        imageSrc: business.image_url,
                        name: business.name,
                        address: business.location.address1,
                        city: business.location.city,
                        state: business.location.state,
                        zipCode: business.location.zip_code,
                        category: business.categories[0].title,
                        rating: business.rating,
                        reviewCount: business.review_count  
                }));
            }
        });
    }
};


export default Yelp;